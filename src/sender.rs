use axolotl;
use curve;


pub enum MessageKey {}
pub enum ChainKey   {}
pub enum KeyState   {}
pub enum KeyRecord  {}

/// A representation of a (group + sender + device) tuple
#[repr(C)]
#[derive(Debug)]
pub struct KeyName {
	pub group_id:     *const u8,
	pub group_id_len: usize,
	pub sender:       axolotl::Address,
}


// sender_key.h //
extern "C" {
	pub fn sender_message_key_create(
			key: *mut *mut MessageKey,
			iteration: u32,
			seed: *mut ::Buffer,
			global_context: *mut ::Context
		) -> ::OkStatus;
	
	pub fn sender_message_key_get_iteration(
			key: *const MessageKey
		) -> u32;
	
	pub fn sender_message_key_get_iv(
			key: *mut MessageKey
		) -> *mut ::Buffer;
	
	pub fn sender_message_key_get_cipher_key(
			key: *mut MessageKey
		) -> *mut ::Buffer;
	
	pub fn sender_message_key_get_seed(
			key: *mut MessageKey
		) -> *mut ::Buffer;
	
	pub fn sender_message_key_destroy(
			key: *mut MessageKey
		);
	
	
	
	pub fn sender_chain_key_create(
			key: *mut *mut ChainKey,
			iteration: u32,
			chain_key: *mut ::Buffer,
			global_context: *mut ::Context
		) -> ::OkStatus;
	
	pub fn sender_chain_key_get_iteration(
			key: *const ChainKey
		) -> u32;
	
	pub fn sender_chain_key_create_message_key(
			key: *mut ChainKey,
			message_key: *mut *mut MessageKey
		) -> ::OkStatus;
	
	pub fn sender_chain_key_create_next(
			key: *mut ChainKey,
			next_key: *mut *mut ChainKey
		) -> ::OkStatus;
	
	pub fn sender_chain_key_get_seed(
			key: *mut ChainKey
		) -> *mut ::Buffer;
	
	pub fn sender_chain_key_destroy(
			key: *mut ChainKey
		);
	
	
	/*
	 * Interface to the sender key store.
	 * These functions will use the callbacks in the provided
	 * axolotl_store_context instance and operate in terms of higher level
	 * library data structures.
	 */
	
	pub fn axolotl_sender_key_store_key(
			context: *mut ::StoreContext,
			sender_key_name: *const KeyName,
			record: *mut KeyRecord) -> ::OkStatus;
	
	pub fn axolotl_sender_key_load_key(
			context: *mut ::StoreContext,
			record: *mut *mut KeyRecord,
			sender_key_name: *const KeyName) -> ::OkStatus;
}


// sender_key_record.h //
extern "C" {
	pub fn sender_key_record_create(
			record: *mut *mut KeyRecord,
			global_context: *mut ::Context
		) -> ::OkStatus;
	
	pub fn sender_key_record_serialize(
			buffer: *mut *mut ::Buffer,
			record: *mut KeyRecord
		) -> ::OkStatus;
	
	pub fn sender_key_record_deserialize(
			record: *mut *mut KeyRecord,
			data: *const u8, len: usize,
			global_context: *mut ::Context
		) -> ::OkStatus;
	
	pub fn sender_key_record_copy(
			record: *mut *mut KeyRecord,
			other_state: *const KeyRecord,
			global_context: *mut ::Context
		) -> ::OkStatus;
	
	
	
	pub fn sender_key_record_is_empty(
			record: *const KeyRecord
		) -> ::Response;
	
	pub fn sender_key_record_get_sender_key_state(
			record: *mut KeyRecord,
			state:  *mut *mut KeyState
		) -> ::OkStatus;
	pub fn sender_key_record_get_sender_key_state_by_id(
			record:	*mut KeyRecord,
			state: *mut *mut KeyState,
			key_id: u32
		) -> ::OkStatus;
	
	pub fn sender_key_record_add_sender_key_state(
			record: *mut KeyRecord,
			id: u32,
			iteration: u32,
			chain_key: *mut ::Buffer,
			signature_key: *mut curve::PublicKey
		) -> ::OkStatus;
	
	pub fn sender_key_record_set_sender_key_state(
			record: *mut KeyRecord,
			id: u32,
			iteration: u32,
			chain_key:          *mut ::Buffer,
			signature_key_pair: *mut curve::KeyPair
		) -> ::OkStatus;
	
	pub fn sender_key_record_destroy(
			record: *mut KeyRecord
		);
}


// sender_key_state.h //
extern "C" {
	pub fn sender_key_state_create(
			state: *mut *mut KeyState,
			id: u32,
			chain_key: *mut ChainKey,
			signature_public_key:  *mut curve::PublicKey,
			signature_private_key: *mut curve::PrivateKey,
			global_context: *mut ::Context
		) -> ::OkStatus;
	
	pub fn sender_key_state_serialize(
			buffer: *mut *mut ::Buffer,
			state:  *mut KeyState
		) -> ::OkStatus;
	
	pub fn sender_key_state_deserialize(
			state: *mut *mut KeyState,
			data: *const u8, len: usize,
			global_context: *mut ::Context
		) -> ::OkStatus;
	
	pub fn sender_key_state_copy(
			state: *mut *mut KeyState,
			other_state: *mut KeyState,
			global_context: *mut ::Context
		) -> ::OkStatus;
	
	
	
	pub fn sender_key_state_get_key_id(
			state: *mut KeyState
		) -> u32;
	
	pub fn sender_key_state_get_chain_key(
			state: *mut KeyState
		) -> *mut ChainKey;
	
	pub fn sender_key_state_set_chain_key(
			state: *mut KeyState,
			chain_key: *mut ChainKey
		);
	
	pub fn sender_key_state_get_signing_key_public(
			state: *mut KeyState
		) -> *mut curve::PublicKey;
	
	pub fn sender_key_state_get_signing_key_private(
			state: *mut KeyState
		) -> *mut curve::PrivateKey;
	
	pub fn sender_key_state_has_sender_message_key(
			state: *const KeyState,
			iteration: u32
		) -> ::Response;
	
	pub fn sender_key_state_add_sender_message_key(
			state:       *mut KeyState,
			message_key: *mut MessageKey
		) -> ::OkStatus;
	
	pub fn sender_key_state_remove_sender_message_key(
			state: *mut KeyState,
			iteration: u32
		) -> *mut MessageKey;
	
	pub fn sender_key_state_destroy(
			state: *mut KeyState
		);
}