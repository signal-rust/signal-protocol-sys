use std::os::raw as types;

// HKDF types
pub enum Context {}



// hkdf.h //
extern "C" {
	pub fn hkdf_create(
			context: *mut *mut Context,
			message_version: types::c_int,
			global_context:  *mut ::Context
		) -> ::OkStatus;
	
	pub fn hkdf_derive_secrets(
			context: *mut Context,
			output:  *mut *mut u8,
			input_key_material: *const u8, input_key_material_len: usize,
			salt: *const u8, salt_len: usize,
			info: *const u8, info_len: usize,
			output_len: usize
		) -> isize;
	
	pub fn hkdf_compare(
			context1: *const Context,
			context2: *const Context
		) -> ::Comparision;
	
	pub fn hkdf_destroy(
			context: *mut Context
		);
}