use std::os::raw as types;

use curve;

// Fingerprint types
pub enum Fingerprint            {}
pub enum DisplayableFingerprint {}
pub enum ScannableFingerprint   {}
pub enum FingerprintGenerator   {}


extern "C" {
	/**
	 * Construct a fingerprint generator for 60 digit numerics.
	 *
	 * @param generator set to a freshly allocated generator instance
	 * @param iterations The number of internal iterations to perform in the process of
	 *                   generating a fingerprint. This needs to be constant, and synchronized
	 *                   across all clients.
	 *                   The higher the iteration count, the higher the security level:
	 *                   - 1024 ~ 109.7 bits
	 *                   - 1400 > 110 bits
	 *                   - 5200 > 112 bits
	 * @param global_context the global library context
	 * @return 0 on success, or negative on failure
	 */
	pub fn fingerprint_generator_create(
			generator: *mut *mut FingerprintGenerator,
			iterations: types::c_int,
			global_context: *const ::Context
		) -> ::OkStatus;
	
	/**
	 * Generate a scannable and displayble fingerprint.
	 *
	 * @param local_stable_identifier The client's "stable" identifier.
	 * @param local_identity_key The client's identity key.
	 * @param remote_stable_identifier The remote party's "stable" identifier.
	 * @param remote_identity_key The remote party's identity key.
	 * @param fingerprint Set to a freshly allocated unique fingerprint for this conversation
	 * @return 0 on success, or negative on failure
	 */
	pub fn fingerprint_generator_create_for(
			generator: *const FingerprintGenerator,
			local_stable_identifier:  *const types::c_char,
			local_identity_key:       *const curve::PublicKey,
			remote_stable_identifier: *const types::c_char,
			remote_identity_key:      *const curve::PublicKey,
			fingerprint_val:          *mut *mut Fingerprint
		) -> ::OkStatus;
	
	pub fn fingerprint_generator_free(
			generator: *mut FingerprintGenerator
		);
	
	
	
	pub fn fingerprint_create(
			fingerprint_val: *mut *mut Fingerprint,
			displayable:     *mut DisplayableFingerprint,
			scannable:       *mut ScannableFingerprint
		) -> ::OkStatus;
	
	pub fn fingerprint_get_displayable(
			fingerprint_val: *mut Fingerprint
		) -> *mut DisplayableFingerprint;
	
	pub fn fingerprint_get_scannable(
			fingerprint_val: *mut Fingerprint
		) -> *mut ScannableFingerprint;
	
	pub fn fingerprint_destroy(
			fingerprint_val: *mut Fingerprint
		);
	
	
	
	pub fn displayable_fingerprint_create(
			displayable:        *mut *mut DisplayableFingerprint,
			local_fingerprint:  *const types::c_char,
			remote_fingerprint: *const types::c_char
		) -> ::OkStatus;
	
	pub fn displayable_fingerprint_local(
			displayable: *mut DisplayableFingerprint
		) -> *const types::c_char;
	
	pub fn displayable_fingerprint_remote(
			displayable: *mut DisplayableFingerprint
		) -> *const types::c_char;
	
	pub fn displayable_fingerprint_text(
			displayable: *mut DisplayableFingerprint
		) -> *const types::c_char;
	
	pub fn displayable_fingerprint_destroy(
			displayable: *mut DisplayableFingerprint
		);
	
	
	
	pub fn scannable_fingerprint_create(
			scannable: *mut *mut ScannableFingerprint,
			version: u32,
			local_stable_identifier:  *const types::c_char,
			local_identity_key:       *const curve::PublicKey,
			remote_stable_identifier: *const types::c_char,
			remote_identity_key:      *const curve::PublicKey
		) -> ::OkStatus;
	
	pub fn scannable_fingerprint_serialize(
			buffer:    *mut *mut ::Buffer,
			scannable: *const ScannableFingerprint
		) -> ::OkStatus;
	
	pub fn scannable_fingerprint_deserialize(
			scannable:  *mut *mut ScannableFingerprint,
			data: *const u8, len: usize,
			global_context: *mut ::Context
		) -> ::OkStatus;
	
	pub fn scannable_fingerprint_get_version(
			scannable: *mut ScannableFingerprint
		) -> u32;
	
	pub fn scannable_fingerprint_get_local_stable_identifier(
			scannable: *mut ScannableFingerprint
		) -> *const types::c_char;
	
	pub fn scannable_fingerprint_get_local_identity_key(
			scannable: *mut ScannableFingerprint
		) -> *mut curve::PublicKey;
	
	pub fn scannable_fingerprint_get_remote_stable_identifier(
			scannable: *mut ScannableFingerprint
		) -> *const types::c_char;
	
	pub fn scannable_fingerprint_get_remote_identity_key(
			scannable: *mut ScannableFingerprint
		) -> *mut curve::PublicKey;
	
	/**
	 * Compare a scanned QR code with what we expect.
	 * @param scannable The local scannable data
	 * @param other_scannable The data from the scanned code
	 * @retval 1 if the scannable codes match
	 * @retval 0 if the scannable codes do not match
	 * @retval AX_ERR_FP_VERSION_MISMATCH if the scanned fingerprint is the wrong version
	 * @retval AX_ERR_FP_IDENT_MISMATCH if the scanned fingerprint is for the wrong stable identifier
	 */
	pub fn scannable_fingerprint_compare(
			scannable:       *mut ScannableFingerprint,
			other_scannable: *const ScannableFingerprint
		) -> ::ResponseStatus;
	
	pub fn scannable_fingerprint_destroy(
			scannable: *mut ScannableFingerprint
	);
}