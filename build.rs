// Location of GIT submodule with C library
static C_LIBRARY_DIR: &'static str = "libsignal-protocol";

fn main() {
	let library_dir = format!("{}/{}",       std::env::var("CARGO_MANIFEST_DIR").unwrap(), C_LIBRARY_DIR);
	let build_dir   = format!("{}/../cmake", std::env::var("OUT_DIR").unwrap());
	
	// Run C library build script
	let status = std::process::Command::new("./build.sh")
		.env("C_LIBRARY_DIR", library_dir)
		.env("C_BUILD_DIR",   build_dir.clone())
		.status()
		.unwrap();
	assert!(status.code().unwrap() == 0, "Build script \"./build.sh\" exited with non-zero exit status!");
	
	// Expose built library to cargo
	println!("cargo:rustc-link-lib=static=signal-protocol-c");
	println!("cargo:rustc-link-search=native={}/src", build_dir);
}
