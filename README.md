# About `libsignal-protocol-c` #

From the project page:

    This is a ratcheting forward secrecy protocol that works in synchronous and
    asynchronous messaging environments.

More general library documentation may be found at OpenWhisperSystem's
[Java Signal library](https://github.com/whispersystems/libsignal-protocol-java).

# Building the binding #

 1. Ensure you have the following packages installed on your system:
    * Downloading the C library and Rust bindings:
      * [`git`](https://git-scm.com/download/)
    * Compiling the C library:
      * [`gcc`](https://gcc.gnu.org/) or [`clang`](https://clang.llvm.org/)
      * [`cmake`](https://cmake.org/download/)
    * Building the Rust bindings:
      * [`rustc` and `cargo`](https://www.rust-lang.org/downloads.html)
 2. Open a Terminal window
 3. Download the bindings using `git`:<br />
    `git clone https://gitlab.com/signal-rust/signal-protocol-sys.git`
 4. Build the Rust library file:<br />
    `cargo build`<br />
    (This will download the C library code using `git`.)

# Usage in other Rust projects #

Add this to your `Cargo.toml`:

    [dependencies]
    signal-protocol-sys = "0.1"

# License #

Licensed under the GPLv3 or (at your option) any later version.

Since OpenWhisperSystem's `libsignal-protocol-c` library uses the GPLv3 as well,
I cannot (in any meaningful way) release this code under any more
linking-friendly license (such as the LGPL).